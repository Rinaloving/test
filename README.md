# ZC-339V 定时开关机

#### 介绍
Uniapp 调用 uniplugin_shutdownandroot-release.arr 定时开关机插件

#### 软件架构
无
#### 代码示例
``` html
<template>
	<view class="content">
		<image class="logo" src="/static/logo.png"></image>
		<view>
			<div>
				<button type="primary" @click="shutDownMachine">关机</button>
				<button type="primary" @click="rebootMachine">重启</button>
				<button type="primary" @click="timingTurnOnMachine(120)">定时120秒后开机</button>
			</div>
		</view>
	</view>
</template>

<script>
// 获取 module
var rebootModule = uni.requireNativePlugin("reboot-module")


export default {
	data() {
		return {
			title: 'Reboot-Module',
			result: 0,
			result1:0,
		};
	},
	onLoad() {},
	methods: {
		shutDownMachine(){
			rebootModule.shutDownMachine();
		},
		rebootMachine(){
			rebootModule.rebootMachine();
		},
		timingTurnOnMachine(times){
			rebootModule.timingTurnOnMachine(times);
		},
	}
};
</script>
```

#### 安装教程

1.  直接引入Uniapp 项目即可

#### 使用说明

1.  插件目录结构

![输入图片说明](static/Snipaste_2024-03-28_09-50-38.png)

2.  package.json配置
```json
{
	"name": "定时开关机",
	"id": "reboot-module",
	"version": "v1.0.2024.03.27",
	"description": "定时开关机插件，联系QQ：1426429456@qq.com",
	"_dp_type": "nativeplugin",
	"_dp_nativeplugin": {
		"android": {
			"plugins": [{
					"type": "module",
					"name": "reboot-module",
					"class": "com.example.uniplugin_shutdownandroot.RebootModule"
				}
			],
			"dependencies": [
				"androidx.appcompat:appcompat:1.0.0-alpha1"
			],
			"integrateType": "aar",
			"minSdkVersion": "19",
			"abis": [
						"armeabi-v7a"
					],
					
			"useAndroidX": true,
			"permissions": [
							"android.permission.REBOOT",
							"android.permission.SHUTDOWN",
							"android.permission.DEVICE_POWER",
							"android.permission.READ_EXTERNAL_STORAGE",
							"android.permission.WRITE_EXTERNAL_STORAG",
							"android.permission.WRITE_SETTINGS"
						]

		}
	}
}
```
3. 运行效果

![输入图片说明](static/Snipaste_2024-03-28_09-30-26.png)

